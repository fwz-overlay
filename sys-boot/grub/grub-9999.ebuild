# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-boot/grub/grub-1.96.ebuild,v 1.4 2008/08/16 15:27:59 vapier Exp $

EBZR_REPO_URI="http://grub.gibibit.com/bzr/"
EBZR_BRANCH="gfxmenu"

inherit mount-boot eutils flag-o-matic toolchain-funcs bzr autotools

DESCRIPTION="GNU GRUB 2 boot loader"
HOMEPAGE="http://www.gnu.org/software/grub/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE="static netboot custom-cflags"

DEPEND=">=sys-libs/ncurses-5.2-r5
	=dev-libs/lzo-2*"
PROVIDE="virtual/bootloader"
RESTRICT="strip"

src_compile() {
	cd "${S}"
	./autogen.sh || die "autogen failed"
	use custom-cflags || unset CFLAGS CPPFLAGS LDFLAGS
	use static && append-ldflags -static
	chmod +x ./configure
	econf || die "configure failed"
	emake
}

src_install() {
	emake DESTDIR="${D}" install || die
	dodoc AUTHORS ChangeLog NEWS README THANKS TODO
}

